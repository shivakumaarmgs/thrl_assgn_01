class CreateBookings < ActiveRecord::Migration
  def change
    create_table :bookings do |t|
      t.references :user, index: true, foreign_key: true
      t.decimal :amount, precision: 5, scale: 2

      t.timestamps null: false
    end
  end
end
