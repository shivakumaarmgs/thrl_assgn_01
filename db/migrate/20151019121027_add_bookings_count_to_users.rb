class AddBookingsCountToUsers < ActiveRecord::Migration
  def change
    add_column :users, :bookings_count, :integer
  end
end
