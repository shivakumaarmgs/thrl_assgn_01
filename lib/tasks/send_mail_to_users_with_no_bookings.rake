namespace :send do

  desc "Send mail to users with no booking history"
  task :mail_seq => :environment do
    Benchmark.bm do |bm|
      Rails.logger.info "====== Seq execution start"
      bm.report('no_async') do
        User.with_no_bookings.each do |user|
          MailUserWithNoBookingsNoAsync.new.send_mail(user)
        end
      end
      Rails.logger.info "====== Seq execution ends"
    end
  end

  desc "Send mail to users with no booking history async"
  task :mail_async => :environment do
    Benchmark.bm do |bm|
      Rails.logger.info "====== Async execution starts"
      bm.report('async_and_pool') do
        mail_pool = MailUserWithNoBookingsAsync.pool(size: User.with_no_bookings.count)
        User.with_no_bookings.each do |user|
          mail_pool.async.send_mail(user)
        end
      end
      Rails.logger.info "====== Async execution ends"
    end
  end
end
