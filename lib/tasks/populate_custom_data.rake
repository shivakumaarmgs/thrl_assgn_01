namespace :db do

  desc "wrapper task"
  task :populate => :environment do
    Rake::Task['db:clear_existing_data'].execute
    Rake::Task['db:populate_users'].execute
    Rake::Task['db:populate_bookings'].execute
  end


  desc "clear database"
  task :clear_existing_data => :environment do
    puts "Clearing Existing Data"
    Booking.delete_all
    User.delete_all
  end

  desc "Create 200 users"
  task :populate_users => :environment do
    puts "Creating Users"
    200.times do |i|
      User.create(
        email: "user#{i}@mail.com",
        first_name: "first_name_#{i}",
        last_name: "last_name_#{i}"
      )
    end
  end

  desc "Create Bookings random number
        of bookings(0-5) for 200 users"
  task :populate_bookings => :environment do
    puts "Creating Bookings"
    User.all.each do |u|
      (0..5).to_a.sample.times do |i|
        u.bookings.create(
          user_id: u.id,
          amount: rand * 1000
        )
      end
    end

  end
end
