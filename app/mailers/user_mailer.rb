class UserMailer < ApplicationMailer

  def marketing_email(user)
    @user = user
    mail(
      from: 'offers@example.com',
      to: @user.email, 
      subject: 'Book a trip and wint 50% cashback'
    )
  end
end
