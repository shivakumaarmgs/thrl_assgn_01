class HomeController < ApplicationController

  def users_with_no_bookings
    @users = User.with_no_bookings
  end

  def users_with_bookings
    Rails.logger.info "------ #{params[:number]}"
    @users = User.with_bookings(params[:number].to_i)
  end
end
