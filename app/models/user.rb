class User < ActiveRecord::Base
  has_many :bookings

  scope :with_bookings, ->(number_of_bookings) {
    return with_no_bookings if number_of_bookings == 0

    # without using counter cache
    joins(:bookings).group("users.id").having("count(*) = ?", number_of_bookings)
    
    # with using counter cache column
    # where("bookings_count = ?", number_of_bookings)
  }

  scope :with_no_bookings, ->() {
    # without using counter cache column
    includes(:bookings).where(bookings: { user_id: nil })

    # with using counter cache column
    # where("bookings_count = 0")
  }
end
