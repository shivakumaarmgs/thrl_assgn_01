class MailUserWithNoBookingsNoAsync

  def send_mail(user)
    UserMailer.marketing_email(user).deliver_later
  end

  def fibonacci(n)
    if n < 2
      n
    else
      fibonacci(n-1) + fibonacci(n-2)
    end
  end

end
